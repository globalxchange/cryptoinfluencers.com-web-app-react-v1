import React ,{useEffect,useState}from 'react'
import Crytoassets from '../../Image/Crytoassets.png'
import inputArraw from '../../Image/inputArraw.png'

import './Homepage.scss'
import {useHistory} from 'react-router-dom';


export default function Homepage() {
    const history = useHistory();
   
    const [changeSize, setChangeSize] = useState("");

    useEffect(()=>{
        let ele = document.getElementById('homepage');
     
 if(ele.offsetWidth<=900)
{
    setChangeSize("What Do You Need Help With?")
}
else{
    setChangeSize("Search Whatever You Need Help With")  
}
        window.addEventListener('resize', handleSize);
        return ()=>{
            window.removeEventListener('resize', handleSize)
        }
    },[changeSize]);
    const handleSize = () =>{
   
    }
  return (
    <div id="homepage" className="homepage"  data-aos="zoom-in">
      <div className="cutonhome" style={{textAlign:"center"}}>
      <img className="logo"  src={Crytoassets} alt=""/>
          <div style={{position:"relative"}} className="topinputdiv">
          <input type="text" name="" placeholder={changeSize} id=""/>
          <img  className="imgsearc" src={inputArraw} alt=""/>
          </div>
 
          <div className="twobuuton">
              <label className="hlogin" onClick={()=> history.push("/assistants")}>Assistants</label>
              <label className="hStarted">Categories</label>
          </div>
    
      </div>
      <div className="bottombrand">
<p className="notshowmobile">Interested In Earning Crypto By Helping Others</p>
<p className="showmobile">Become An Assistant Today</p>
  <label >Click Here</label>
          </div>
    </div>
  )
}
