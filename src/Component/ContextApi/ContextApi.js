import React, { Component, createContext } from 'react'
export const Agency = createContext()
export class AgencyProvider extends Component {

  state = {
  }

  render() {
    
    return (
      <Agency.Provider
        value={{
          ...this.state,
      
          
        }
        }
      >
        {/* <ContextDevTool context={Agency} id="uniqContextId" displayName="Context Display Name" /> */}
        {this.props.children}
      </Agency.Provider>
    )
  }
}
